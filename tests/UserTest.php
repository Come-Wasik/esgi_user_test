<?php

use Nolikein\User;
use PHPUnit\Framework\TestCase;

class UserTest extends TestCase
{
    public function testWhenCheckUser()
    {
        /** @var User A user entity */
        $user = new User();

        /** @var string[] All data which will added to the user */
        $addedData = [
            'email' => 'truc@machin.org',
            'firstname' => 'Henry',
            'lastname' => 'Passoire',
            'password' => 'jdsijdisjdisjij'
        ];

        // Tests default values
        $this->assertEmpty($user->getEmail());
        $this->assertEmpty($user->getFirstname());
        $this->assertEmpty($user->getLastname());
        $this->assertEmpty($user->getPassword());

        // Tests when using setters and getters
        foreach ($addedData as $propertyName => $propertyValue) {
            /** @var string The current user setter method selected */
            $setter = 'set' . ucfirst($propertyName);
            $user->$setter($propertyValue);

            /** @var string The current user getter method selected */
            $getter = 'get' . ucfirst($propertyName);
            $this->assertEquals($propertyValue, $user->$getter($propertyValue));
        }
    }

    public function testCheckUserMethod()
    {
        /** @var User A user entity */
        $user = new User();

        /** @var string[] All data which will added to the user */
        $addedData = [
            'email' => 'truc@machin.org',
            'firstname' => 'Henry',
            'lastname' => 'Passoire',
            'password' => 'jdsijdisjdisjij'
        ];

        // Assert is false by default
        $this->assertFalse($user->isValid());

        foreach ($addedData as $propertyName => $propertyValue) {
            /** @var string The current user setter method selected */
            $setter = 'set' . ucfirst($propertyName);
            $user->$setter($propertyValue);
        }

        // Assert the object is valid
        $this->assertTrue($user->isValid());
    }

    public function testWhenSetsEmptyEmail()
    {
        $this->expectException(InvalidArgumentException::class);

        /** @var User A user entity */
        $user = new User();

        $user->setEmail('');
    }

    public function testWhenSetsBadEmail()
    {
        $this->expectException(InvalidArgumentException::class);

        /** @var User A user entity */
        $user = new User();

        $user->setEmail('truc');
    }

    public function testWhenSetsEmptyFirstname()
    {
        $this->expectException(InvalidArgumentException::class);

        /** @var User A user entity */
        $user = new User();

        $user->setFirstname('');
    }

    public function testWhenSetsEmptyLastname()
    {
        $this->expectException(InvalidArgumentException::class);

        /** @var User A user entity */
        $user = new User();

        $user->setLastname('');
    }

    public function testWhenSetsEmptyPassword()
    {
        $this->expectException(InvalidArgumentException::class);

        /** @var User A user entity */
        $user = new User();

        $user->setPassword('');
    }
}
