<?php

namespace Nolikein;

use InvalidArgumentException;

/**
 * Define a user entity
 */
class User implements UserInterface
{
    /** @var string $email The user email */
    protected string $email = '';

    /** @var string $firstname The user firstname */
    protected string $firstname = '';

    /** @var string $lastname The user lastname */
    protected string $lastname = '';

    /** @var string $password The user password */
    protected string $password = '';

    public function setEmail(string $email): void
    {
        if (empty($email)) {
            throw new InvalidArgumentException('The email cannot be empty', 500);
        }

        if (\filter_var($email, FILTER_VALIDATE_EMAIL) === false) {
            throw new InvalidArgumentException('The email is not valid', 500);
        }

        $this->email = strtolower($email);
    }

    public function setFirstname(string $firstname): void
    {
        if (empty($firstname)) {
            throw new InvalidArgumentException('The firstname cannot be empty', 500);
        }

        $this->firstname = $firstname;
    }

    public function setLastname(string $lastname): void
    {
        if (empty($lastname)) {
            throw new InvalidArgumentException('The lastname cannot be empty', 500);
        }

        $this->lastname = $lastname;
    }

    public function setPassword(string $password): void
    {
        if (empty($password)) {
            throw new InvalidArgumentException('The password cannot be empty', 500);
        }

        $this->password = $password;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function getFirstname(): string
    {
        return $this->firstname;
    }

    public function getLastname(): string
    {
        return $this->lastname;
    }

    public function getPassword(): string
    {
        return $this->password;
    }

    public function isValid(): bool
    {
        if (
            empty($this->email)
            || \filter_var($this->email, FILTER_VALIDATE_EMAIL) === false
            || empty($this->firstname)
            || empty($this->lastname)
            || empty($this->password)

        ) {
            return false;
        }

        return true;
    }
}
