<?php

namespace Nolikein;

interface UserInterface
{
    /**
     * Sets a new email to the user
     * 
     * @param string $email The user email
     * 
     * @throws InvalidArgumentException The email is empty or not valid
     */
    public function setEmail(string $email): void;

    /**
     * Sets a new username to the user
     * 
     * @param string $username The username of the user
     * 
     * @throws InvalidArgumentException The username is empty
     */
    public function setFirstname(string $username): void;

    /**
     * Sets a new lastname to the user
     * 
     * @param string $lastname The lastname of the user
     * 
     * @throws InvalidArgumentException The lastname is empty
     */
    public function setLastname(string $lastname): void;

    /**
     * Sets a new password to the user
     * 
     * @param string $password The password of the user
     * 
     * @throws InvalidArgumentException The password is empty
     */
    public function setPassword(string $password): void;

    /**
     * Gets the user email
     * 
     * @return string The user email
     */
    public function getEmail(): string;

    /**
     * Gets the user firstname
     * 
     * @return string The user firstname
     */
    public function getFirstname(): string;

    /**
     * Gets the user lastname
     * 
     * @return string The user lastname
     */
    public function getLastname(): string;

    /**
     * Gets the user password
     * 
     * @return string The user password
     */
    public function getPassword(): string;

    /**
     * Return if the current user object is valid
     * 
     * @return bool True if the current object is valid, false if is not.
     */
    public function isValid(): bool;
}
